/** @type {import('tailwindcss').Config} */
export default {
  content: [],
  purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        darkyellow: "#FFB800",
      },
    },
    fontFamily: {
      sans: ["Roboto", "Sans-serif"],
    },
  },
  plugins: [],
};
